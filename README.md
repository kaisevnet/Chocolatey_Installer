# Chocolatey Installer

## Purpose

This is a small executable designed to install [Chocolatey](chocolatey.org), [Chocolatey GUI](https://chocolatey.org/packages/ChocolateyGUI), and create a shortcut for Chocolatey GUI on the user's desktop. This is meant for users who can't wrap their head around a terminal, and would struggle with anyting more than 'double click on the thing'.

## Usage

1. Download the latest [release](https://gitlab.com/kaisevnet/Chocolatey_Installer/-/releases)
2. Double click on the file to run it
3. Click on **Yes** for User Account Control
4. When the installer is done, press enter to close it

Now you can access all the software Chocolatey has to offer through a convenient shortcut on your desktop.